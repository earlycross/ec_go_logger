package eclog

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
	"testing"
)

func TestLogging(t *testing.T) {
	var outW bytes.Buffer
	var errW bytes.Buffer

	Init(&outW, &errW, log.LstdFlags|log.Lshortfile)
	globalLogger.SetMinLevel(LogLvInfo)

	Debug("Test", "debug: %d", 1)
	Info("Test", "info: %s", "hoge")
	Warn("Test", "warn")
	Error("Test", "error: %v", errors.New("error"))

	logger := NewLocalLogger("Local")
	logger.Info("info: %s", "fuga")

	out := outW.String()
	err := errW.String()

	fmt.Println("out:")
	fmt.Println(out)

	fmt.Println("err:")
	fmt.Println(err)
}

func TestSingleOutput(t *testing.T) {
	var outW bytes.Buffer

	InitWithSingleOutput(&outW, log.Lshortfile)
	SetMinLevel(LogLvInfo)

	Debug("Single", "debug: %d", 1)
	Info("Single", "info: %s", "hoge")
	Warn("Single", "warn: %s", "fuga")
	Error("Single", "error: %v", errors.New("unexpected error"))

	fmt.Println("out:")
	fmt.Println(outW.String())
}

func TestCSVOutput(t *testing.T) {
	var outW bytes.Buffer
	var errW bytes.Buffer

	InitCSV(&outW, &errW)
	SetMinLevel(LogLvDebug)

	Debug("CSVTest", "debug: %s", "hoge,hoge")
	Info("CSVTest", "info: %s", "fugafuga")
	Warn("CSVTest", "warn")
	Error("CSVTest", "error: %v", errors.New("xxx"))
	Fatal("CSVTest", "fatal")

	fmt.Println("out:")
	fmt.Println(outW.String())

	fmt.Println("err:")
	fmt.Println(errW.String())
}

func TestCSVOutputRemovesNewLineFromBody(t *testing.T) {
	var outW bytes.Buffer

	InitCSV(&outW, ioutil.Discard)
	SetMinLevel(LogLvDebug)

	// bodyが改行を含むようなログを出力
	Error("CSVNewLineTest", "error: %v", errors.New(`error occured!
cause: hoge
aaaaa`))

	// 正しくbodyから改行が取り除かれていれば出力は1行のはず
	numLines := len(strings.Split(strings.TrimSpace(outW.String()), "\n"))
	if numLines != 1 {
		t.Errorf("output should be a single line, but #line is: %d", numLines)
	}
}
