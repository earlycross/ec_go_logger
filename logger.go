package eclog

import "sync"

type Logger interface {
	Debug(cmdName string, fmtStr string, args ...interface{})
	Info(cmdName string, fmtStr string, args ...interface{})
	Warn(cmdName string, fmtStr string, args ...interface{})
	Error(cmdName string, fmtStr string, args ...interface{})
	Fatal(cmdName string, fmtStr string, args ...interface{})
	WithLevel(level LogLevel, cmdName string, fmtStr string, args ...interface{})

	SetMinLevel(minLv LogLevel)
}

// LogLevel ... ログレベル
type LogLevel int

// ログレベルの定数
const (
	LogLvDebug LogLevel = iota
	LogLvInfo
	LogLvWarn
	LogLvError
	LogLvFatal
)

var mapLogLevel = map[string]LogLevel{
	"DEBUG": LogLvDebug,
	"INFO":  LogLvInfo,
	"WARN":  LogLvWarn,
	"ERROR": LogLvError,
	"FATAL": LogLvFatal,
}

func (lv LogLevel) String() string {
	for s, l := range mapLogLevel {
		if lv == l {
			return s
		}
	}
	return "???"
}

// LogLevelFromString ... ログレベルの文字列をLogLevelの定数に変換
func LogLevelFromString(strLv string) LogLevel {
	lv, ok := mapLogLevel[strLv]
	if !ok {
		return LogLvDebug
	}
	return lv
}

var once sync.Once
var globalLogger Logger

func Debug(cmdName string, fmtStr string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Debug(cmdName, fmtStr, args...)
	}
}

func Info(cmdName string, fmtStr string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Info(cmdName, fmtStr, args...)
	}
}

func Warn(cmdName string, fmtStr string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Warn(cmdName, fmtStr, args...)
	}
}

func Error(cmdName string, fmtStr string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Error(cmdName, fmtStr, args...)
	}
}

func Fatal(cmdName string, fmtStr string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Fatal(cmdName, fmtStr, args...)
	}
}

func WithLevel(level LogLevel, cmdName string, fmtStr string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.WithLevel(level, cmdName, fmtStr, args...)
	}
}

func SetMinLevel(level LogLevel) {
	globalLogger.SetMinLevel(level)
}

type localLogger struct {
	cmdName string
}

func NewLocalLogger(cmdName string) *localLogger {
	return &localLogger{cmdName}
}

func (l *localLogger) Debug(fmtStr string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Debug(l.cmdName, fmtStr, args...)
	}
}

func (l *localLogger) Info(fmtStr string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Info(l.cmdName, fmtStr, args...)
	}
}

func (l *localLogger) Warn(fmtStr string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Warn(l.cmdName, fmtStr, args...)
	}
}

func (l *localLogger) Error(fmtStr string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Error(l.cmdName, fmtStr, args...)
	}
}

func (l *localLogger) Fatal(fmtStr string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.Fatal(l.cmdName, fmtStr, args...)
	}
}

func (l *localLogger) WithLevel(level LogLevel, fmtStr string, args ...interface{}) {
	if globalLogger != nil {
		globalLogger.WithLevel(level, l.cmdName, fmtStr, args...)
	}
}
