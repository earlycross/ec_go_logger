package eclog

import (
	"fmt"

	"github.com/kardianos/service"
)

// serviceLogger is wrapper for github.com/kardianos/service's Logger.
type serviceLogger struct {
	srvLogger service.Logger
	minLevel  LogLevel
}

// InitWithServiceLogger initialize eclog with kardianos/service's system logger.
func InitWithServiceLogger(srvLogger service.Logger) {
	once.Do(func() {
		globalLogger = &serviceLogger{
			srvLogger: srvLogger,
			minLevel:  LogLvDebug,
		}
	})
}

func (l *serviceLogger) SetMinLevel(minLv LogLevel) {
	if l != nil {
		l.minLevel = minLv
	}
}

func srvLogFormat(level LogLevel, cmdName string, fmtStr string) string {
	return fmt.Sprintf("[%-5s] [%s] %s", level, cmdName, fmtStr)
}

func (l *serviceLogger) writeLogBody(level LogLevel, cmdName string, fmtStr string, args ...interface{}) {
	if level >= l.minLevel {
		logFmt := srvLogFormat(level, cmdName, fmtStr)
		var logFn func(string, ...interface{}) error

		switch level {
		case LogLvDebug, LogLvInfo:
			logFn = l.srvLogger.Infof
		case LogLvWarn:
			logFn = l.srvLogger.Warningf
		case LogLvError, LogLvFatal:
			logFn = l.srvLogger.Errorf
		}

		_ = logFn(logFmt, args...)
	}
}

func (l *serviceLogger) Debug(cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(LogLvDebug, cmdName, fmtStr, args...)
}

func (l *serviceLogger) Info(cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(LogLvInfo, cmdName, fmtStr, args...)
}

func (l *serviceLogger) Warn(cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(LogLvWarn, cmdName, fmtStr, args...)
}

func (l *serviceLogger) Error(cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(LogLvError, cmdName, fmtStr, args...)
}

func (l *serviceLogger) Fatal(cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(LogLvFatal, cmdName, fmtStr, args...)
}

func (l *serviceLogger) WithLevel(level LogLevel, cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(level, cmdName, fmtStr, args...)
}
