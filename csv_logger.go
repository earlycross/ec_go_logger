package eclog

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"sync"
	"time"
)

type csvLogger struct {
	lv2Writer map[LogLevel]*csv.Writer
	minLevel  LogLevel
	mu        sync.Mutex
}

// InitCSV ... CSV形式で出力するLoggerを初期化
func InitCSV(outW io.Writer, errW io.Writer) {
	once.Do(func() {
		logger := &csvLogger{
			lv2Writer: make(map[LogLevel]*csv.Writer),
		}
		var destW io.Writer
		for lv, dest := range mapLogDestPerLevel {
			switch dest {
			case logDestOut:
				destW = outW
			case logDestErr:
				destW = errW
			case logDestBoth:
				destW = io.MultiWriter(outW, errW)
			}
			logger.lv2Writer[lv] = csv.NewWriter(destW)
		}
		globalLogger = logger
	})
}

// InitDefaultCSV ... 標準出力と標準エラーにCSV形式で出力するLoggerを初期化
func InitDefaultCSV() {
	InitCSV(os.Stdout, os.Stderr)
}

// SetMinLevel ...
func (l *csvLogger) SetMinLevel(minLv LogLevel) {
	if l != nil {
		l.minLevel = minLv
	}
}

func (l *csvLogger) writeLogBody(level LogLevel, cmdName string, fmtStr string, args ...interface{}) {
	if level >= l.minLevel {
		// ロガー呼び出し箇所を取得
		_, file, line, ok := runtime.Caller(3)
		if !ok {
			file = "???"
			line = 0
		}

		l.mu.Lock()
		defer l.mu.Unlock()

		fields := []string{
			fmt.Sprint(time.Now().UnixNano() / 1e6),         // 時刻(ミリ秒単位)
			level.String(),                                  // ログレベル文字列
			fmt.Sprintf("%s:%d", filepath.Base(file), line), // ロガー呼び出し箇所
			cmdName, // コマンド名(ラベル)
			removeNewlines(fmt.Sprintf(fmtStr, args...)), // ログ本体
		}
		_ = l.lv2Writer[level].Write(fields)
		l.lv2Writer[level].Flush()
	}
}

// 改行を取り除くReplacer ログの可読性のためスペースに置き換える
var nlRemover = strings.NewReplacer(
	"\r\n", " ",
	"\r", " ",
	"\n", " ",
)

// 引数の文字列からすべての改行を取り除く
func removeNewlines(s string) string {
	return nlRemover.Replace(s)
}

// Debug ...
func (l *csvLogger) Debug(cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(LogLvDebug, cmdName, fmtStr, args...)
}

// Info ...
func (l *csvLogger) Info(cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(LogLvInfo, cmdName, fmtStr, args...)
}

// Warn ...
func (l *csvLogger) Warn(cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(LogLvWarn, cmdName, fmtStr, args...)
}

// Error ...
func (l *csvLogger) Error(cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(LogLvError, cmdName, fmtStr, args...)
}

// Fatal ...
func (l *csvLogger) Fatal(cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(LogLvFatal, cmdName, fmtStr, args...)
}

// WithLevel ... ログレベルを動的に指定してログを書き出す
func (l *csvLogger) WithLevel(level LogLevel, cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(level, cmdName, fmtStr, args...)
}
