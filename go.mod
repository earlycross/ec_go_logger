module bitbucket.org/earlycross/ec_go_logger

go 1.12

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kardianos/service v1.0.0
)
