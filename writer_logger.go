package eclog

import (
	"fmt"
	"io"
	"log"
	"os"
)

type logDest int

const (
	logDestOut logDest = iota
	logDestErr
	logDestBoth
)

var mapLogDestPerLevel = map[LogLevel]logDest{
	LogLvDebug: logDestOut,
	LogLvInfo:  logDestOut,
	LogLvWarn:  logDestBoth,
	LogLvError: logDestBoth,
	LogLvFatal: logDestBoth,
}

type writerLogger struct {
	lv2Logger map[LogLevel]*log.Logger
	minLevel  LogLevel
}

// Init ... Logger初期化
func Init(outW io.Writer, errW io.Writer, logFlag int) {
	once.Do(func() {
		logger := new(writerLogger)
		logger.lv2Logger = make(map[LogLevel]*log.Logger)

		var destW io.Writer
		for lv, dest := range mapLogDestPerLevel {
			switch dest {
			case logDestOut:
				destW = outW
			case logDestErr:
				destW = errW
			case logDestBoth:
				destW = io.MultiWriter(outW, errW)
			}
			logger.lv2Logger[lv] = log.New(destW, fmt.Sprintf("[%-5s] ", lv.String()), logFlag)
		}
		globalLogger = logger
	})
}

// InitDefault ... 標準出力と標準エラーに出力するLoggerを初期化
func InitDefault(logFlag int) {
	Init(os.Stdout, os.Stderr, logFlag)
}

// InitWithSingleOutput ... 指定した単一の出力先に出力するLoggerを初期化
// Lambdaのログ出力での利用を想定
func InitWithSingleOutput(outW io.Writer, logFlag int) {
	once.Do(func() {
		logger := new(writerLogger)
		logger.lv2Logger = make(map[LogLevel]*log.Logger)

		for _, lv := range mapLogLevel {
			logger.lv2Logger[lv] = log.New(outW, fmt.Sprintf("[%-5s] ", lv.String()), logFlag)
		}
		globalLogger = logger
	})
}

// SetMinLevel ... 出力する最小のログレベルを設定する
func (l *writerLogger) SetMinLevel(minLv LogLevel) {
	if l != nil {
		l.minLevel = minLv
	}
}

func logFormat(cmdName string, fmtStr string) string {
	return "[" + cmdName + "] " + fmtStr
}

func (l *writerLogger) writeLogBody(level LogLevel, cmdName string, fmtStr string, args ...interface{}) {
	if level >= l.minLevel {
		_ = l.lv2Logger[level].Output(4, fmt.Sprintf(logFormat(cmdName, fmtStr), args...))
	}
}

// Debug ...
func (l *writerLogger) Debug(cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(LogLvDebug, cmdName, fmtStr, args...)
}

// Info ...
func (l *writerLogger) Info(cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(LogLvInfo, cmdName, fmtStr, args...)
}

// Warn ...
func (l *writerLogger) Warn(cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(LogLvWarn, cmdName, fmtStr, args...)
}

// Error ...
func (l *writerLogger) Error(cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(LogLvError, cmdName, fmtStr, args...)
}

// Fatal ...
func (l *writerLogger) Fatal(cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(LogLvFatal, cmdName, fmtStr, args...)
}

// WithLevel ... ログレベルを動的に指定してログを書き出す
func (l *writerLogger) WithLevel(level LogLevel, cmdName string, fmtStr string, args ...interface{}) {
	l.writeLogBody(level, cmdName, fmtStr, args...)
}
